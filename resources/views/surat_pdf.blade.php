<!DOCTYPE html>
<html>

<head>
    <title>Surat Perintah</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    @php
    $rt = $pegawai->body_surat == null ? '' :$pegawai->body_surat->rt;
    $rw = $pegawai->body_surat == null ? '' :$pegawai->body_surat->rw;
    $kel = $pegawai->body_surat == null ? '' :$pegawai->body_surat->kel;
    $kec = $pegawai->body_surat == null ? '' :$pegawai->body_surat->kec;
    $kab = $pegawai->body_surat == null ? '' :$pegawai->body_surat->kab;
    @endphp
    <table>
        <tr>
            <td width="60"> <img src="{{public_path('logo.png')}}" width="70"></td>
            <td style="vertical-align: top;">RUKUN TETANGGA {{$pegawai->body_surat == null ? '' :$pegawai->body_surat->rt}}/{{$pegawai->body_surat == null ? '' :$pegawai->body_surat->rw}}<br>
                KELURAHAN {{$pegawai->body_surat == null ? '' : strtoupper($pegawai->body_surat->kel)}} KECAMATAN {{$pegawai->body_surat == null ? '' : strtoupper($pegawai->body_surat->kec)}} <br>
                KOTA ADMINISTRASI {{$pegawai->body_surat == null ? '' :strtoupper($pegawai->body_surat->kab)}}
            </td>
        </tr>
    </table>
    <br>
    <br>

    <center>
        <H4><u>SURAT PENGATAR</u></H4>
        <p>Nomor : {{date('Y')}}/{{$pegawai->body_surat == null ? '' :$pegawai->body_surat->rt}}/{{strtoupper(substr($pegawai->body_surat == null ? '' :$pegawai->body_surat->kel, 0, 3))}}/{{$pegawai->id}}</p>
    </center>
    <p>
        Yang bertanda tangan di bawah ini Ketua RT. {{$rt}} RW. {{$rw}} Kelurahan {{ucfirst($kel)}} Kecamatan {{ucfirst($kec)}} Kabupaten/Kota {{ucfirst($kab)}} dengan ini menerangkan bahwa:
    </p>
    <table class='table table-borderless'>
        <tr>
            <td width="60">Nama</td>
            <td width="10">:</td>
            <td>{{$pegawai->user->name}}</td>
        </tr>
        <tr>
            <td width="60">Jenis Kelamin</td>
            <td width="10">:</td>
            <td>{{$pegawai->user->profile->jenis_kelamin}}</td>
        </tr>
        <tr>
            <td width="60">Pekerjaan</td>
            <td width="10">:</td>
            <td>{{$pegawai->user->profile->pekerjaan}}</td>
        </tr>
        <tr>
            <td width="60">Alamat</td>
            <td width="10">:</td>
            <td>{{$pegawai->user->profile->alamat}}</td>
        </tr>

    </table>
    <p>Orang tersebut diatas, adalah benar-benar warga kami dan berkelakuan baik di masyarakat. Surat keterangan ini dibuat untuk keperluan pengurusan {{$pegawai->jenis_surat->nama}}</p>

    <table class='table  '>
        <tr>
            <td> Jakarta, ........</td>
            <td width="40%"></td>
            <td> Jakarta, {{Carbon\Carbon::parse($pegawai->updated_at)->formatLocalized('%A %d %B %Y')}}</td>
        </tr>
        <tr>
            <td>

            </td>
            <td width="40%"></td>
            <td height="10%">
                @if($pegawai->status == 'Berhasil')
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge(public_path('logo.png'), 0.2, true)
                        ->size(150)->errorCorrection('H')
                        ->generate('Telah Di tanda Tangani OLEH RT')) !!} "></td>
            @else
            <br>
            <br>
            <br>
            @endif
        </tr>
        <tr>
            <td style="text-align: center;">{{$pegawai->body_surat->nama_rw}} <br>
                Pengurus RW {{$rw}}
            </td>
            <td width="40%"></td>
            <td style="text-align: center;">{{$pegawai->body_surat->nama_rt}} <br>
                Pengurus RT {{$rt}}
            </td>
        </tr>
</body>

</html>