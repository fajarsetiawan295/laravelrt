<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadBuktiTagihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_bukti_tagihan', function (Blueprint $table) {
            $table->id();
            $table->string('foto');
            $table->string('nomor_rekening');
            $table->string('atas_nama');
            $table->string('tagihan_id');
            $table->string('status');
            $table->bigInteger('users_id');
            $table->string('key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_bukti_tagihan');
    }
}
