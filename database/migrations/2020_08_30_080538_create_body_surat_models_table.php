<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodySuratModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_surat', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('rt');
            $table->string('rw');
            $table->string('kel');
            $table->string('kec');
            $table->string('kab');
            $table->string('nama_rt');
            $table->string('nama_rw');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_surat');
    }
}
