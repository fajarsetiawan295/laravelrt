<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DonasiUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('donasi_upload', function (Blueprint $table) {
            $table->id();
            $table->string('category_id');
            $table->string('informasi_id');
            $table->string('norek');
            $table->string('atasnama');
            $table->string('foto');
            $table->string('users_id');
            $table->string('key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('donasi_upload');
    }
}
