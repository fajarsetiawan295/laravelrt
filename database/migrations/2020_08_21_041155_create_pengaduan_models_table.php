<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengaduanModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_pengaduan_id');
            $table->text('deskripsi');
            $table->string('foto')->nullable();
            $table->string('status');
            $table->bigInteger('users_id');
            $table->timestamps();
        });
        Schema::create('status_pengaduan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pengaduan_id');
            $table->text('deskripsi');
            $table->string('foto')->nullable();
            $table->string('status');
            $table->bigInteger('users_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduan');
        Schema::dropIfExists('status_pengaduan');
    }
}
