<?php

namespace App\Helpers;

use App\Model\fcm_tokenmodel;
use App\Model\Product\VoucherModel;
use App\Model\TokenFcmModel;
use App\Model\Transaksi\TransaksiModel;
use App\Model\WargaModel;
use App\transfermodel;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Guzzle\Http\Exception\BadResponseException;


class ResponeHelper
{
    public static function EncriptKeyCompany($request, $key)
    {
        $secretprefix = "Qwert123";
        $secretkey = $key;

        $hashed = hash('sha256', $secretprefix . $secretkey);
        $encryption_key = substr($hashed, 0, 32);
        error_log($encryption_key);
        // dd($encryption_key);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        error_log("ini iv");
        error_log($iv);
        $data = $request;
        $enc = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        $en = substr($enc, 0, 8);

        return $en;
    }
    public static function cekKey()
    {
        # code...
        $data = WargaModel::where('status', 'Active')->where('users_id', Auth::user()->id)->first();
        return $data->key;
    }
    public static function fcmtoken($email, $title, $body, $type)
    {
        # code...
        // dd($header);

        if ($type == 'all') {
            $getAllToken = TokenFcmModel::get()->map(function ($value) {
                return $value->token;
            })->toArray();
        } else {
            $getAllToken = TokenFcmModel::where('email', $email)->get()->map(function ($value) {
                return $value->token;
            })->toArray();
        }

        $array = [
            "registration_ids" => $getAllToken,
            "notification" => [
                "title" => $title,
                "body" => $body
            ]
        ];
        $bod = json_encode($array);

        $client = new Client();
        $url = 'https://fcm.googleapis.com/fcm/send';

        try {
            $response = $client->post($url, [
                'http_errors' => false,
                'body' => $bod,
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => "key=AAAALNhxIGc:APA91bGsiWcdWZgAz77Ew-TI3UJHooRA0PMc6WJbpEKQMkJNIuKUUzMqQDZDC2irld16qHInQL3dECCt8VXdbt89hTwcqYETkPLsjnxWcdT2NT7Z5CIxqTGCK9O-g1uplhP6BVdXzKna"
                ]

            ]);
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            return false;
        } catch (\Exception $exception) {
            return false;
        }
        $respons = $response->getBody()->getContents();
        return $respons;
    }


    public static function ResponValidator($validator)
    {
        $array = "";
        foreach ($validator->errors()->all() as $key => $value) {
            $array .=  $value . ',';
        }
        # code...
        return   response()->json([
            'code' => 422,
            "status" => false,
            'message' =>  $array,
        ], 422);
    }
    public static function GetDataBerhasil($data)
    {
        # code...
        return   response()->json([
            'code' => 200,
            "status" => true,
            'message' => 'Berhasil Mengambil data',
            'data' => $data
        ], 200);
    }

    public static function CreteorUpdateBerhasil($data, $message)
    {
        # code...
        return   response()->json([
            'code' => 200,
            "status" => true,
            'message' => $message,
            'data' => $data
        ], 200);
    }

    public static function worngdata($data)
    {
        # code...
        return   response()->json([
            'code' =>  401,
            "status" => false,
            'message' => $data,
        ], 401);
    }



    public static function SevicesErorr($data)
    {
        # code...
        return   response()->json([
            'code' =>  500,
            "status" => false,
            'message' => $data,
        ], 500);
    }

    public static function badRequest($msg)
    {
        return response()->json([
            'status' => false,
            'code' => 400,
            'message' => $msg,
        ], 400);
    }

    public static function NotFound($msg)
    {
        return response()->json([
            'status' => false,
            'code' => 404,
            'message' => $msg,
        ], 404);
    }

    public static function Forbidden($msg)
    {
        return response()->json([
            'status' => false,
            'code' => 403,
            'message' => $msg,
        ], 403);
    }
}
