<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SuratModel extends Model
{
    //
    protected $table = 'surat_models';
    protected $fillable = [
        'jenis_surat_id',
        'users_id',
        'key',
        'keterangan',
        'status'
    ];

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
    public function jenis_surat()
    {
        # code...
        return $this->belongsTo(JenisSuratModel::class, 'jenis_surat_id', 'id');
    }
    public function body_surat()
    {
        # code...
        return $this->belongsTo(BodySuratModel::class, 'key', 'key');
    }
}
