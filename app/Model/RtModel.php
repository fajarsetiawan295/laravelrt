<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RtModel extends Model
{
    //
    protected $table = 'rt';
    protected $fillable = [
        'key',
        'users_id',
    ];

    public function warga()
    {
        # code...
        return $this->hasMany(WargaModel::class, 'key', 'key');
    }
}
