<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisSuratModel extends Model
{
    //
    protected $table = 'jenis_surat';
    protected $fillable = [
        'nama',
    ];
}
