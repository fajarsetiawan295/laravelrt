<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class KomentarInformasiModel extends Model
{
    //
    protected $table = 'komentar_informasi';
    protected $fillable = [
        'users_id',
        'informasi_id',
        'komentar',
    ];

    public function informasi()
    {
        # code...
        return $this->belongsTo(KomentarInformasiModel::class, 'informasi_id', 'id');
    }
    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
