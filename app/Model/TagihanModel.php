<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TagihanModel extends Model
{
    //
    protected $table = 'tagihan';
    // protected $dates = ['tanggal'];
    protected $fillable = [
        'jenis_tagihan',
        'nominal',
        'tanggal',
        'rekening_id',
        'key',
    ];
    public function rekening()
    {
        # code...
        return $this->belongsTo(RekeningModel::class, 'rekening_id', 'id');
    }

    public function uploadbukti()
    {
        # code...
        return $this->hasMany(UploadBuktiTagihanModel::class, 'rekening_id');
    }
}
