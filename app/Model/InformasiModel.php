<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class InformasiModel extends Model
{
    //
    protected $table = 'informasi';
    protected $fillable = [
        'category_id',
        'foto',
        'keterangan',
        'judul_informasi',
        'users_id',
        'key'
    ];

    public function category()
    {
        # code...
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function komentar()
    {
        # code...
        return $this->hasMany(KomentarInformasiModel::class, 'informasi_id');
    }
}
