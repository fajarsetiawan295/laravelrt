<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RekeningModel extends Model
{
    //
    protected $table = 'rekening';
    protected $fillable = [
        'atas_nama',
        'nomor_rekening',
        'users_id',
        'nama_bank'
    ];
}
