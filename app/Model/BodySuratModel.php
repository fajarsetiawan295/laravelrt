<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BodySuratModel extends Model
{
    //
    protected $table = 'body_surat';
    protected $fillable = [
        'key',
        'rt',
        'rw',
        'kel',
        'kec',
        'kab',
        'nama_rt',
        'nama_rw',
    ];
}
