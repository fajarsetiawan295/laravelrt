<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WargaModel extends Model
{
    //
    protected $table = 'warga';
    protected $fillable = [
        'users_id',
        'type',
        'key',
        'status',
    ];
    public function rt()
    {
        # code...
        return $this->belongsTo(RtModel::class, 'key', 'key');
    }
    public function users()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
