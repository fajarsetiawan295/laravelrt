<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusPengaduanModel extends Model
{
    //
    protected $table = 'status_pengaduan';
    protected $fillable = [
        'pengaduan_id',
        'deskripsi',
        'foto',
        'status',
        'users_id',
    ];

    public function pengaduan()
    {
        # code...
        return $this->belongsTo(PengaduanModel::class, 'pengaduan_id', 'id');
    }
    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
