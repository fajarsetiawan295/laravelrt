<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Donasi_UploadModel extends Model
{
    //
    protected $table = 'donasi_upload';
    protected $fillable = [
        'category_id',
        'informasi_id',
        'norek',
        'atasnama',
        'foto',
        'users_id',
        'key',
    ];

    public function category()
    {
        # code...
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }

    public function informasi()
    {
        # code...
        return $this->belongsTo(InformasiModel::class, 'informasi_id', 'id');
    }

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}
