<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TokenFcmModel extends Model
{
    //
    protected $table = 'token_fcm';
    protected $fillable = [
        'email',
        'token',
    ];
}
