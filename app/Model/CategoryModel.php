<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    //
    protected $table = 'category';
    protected $fillable = [
        'judul',
    ];

    public function informasi()
    {
        # code...
        return $this->hasMany(InformasiModel::class, 'category_id');
    }
}
