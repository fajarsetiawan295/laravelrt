<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PengaduanModel extends Model
{
    //
    protected $table = 'pengaduan';
    protected $fillable = [
        'category_pengaduan_id',
        'deskripsi',
        'foto',
        'status',
        'users_id',
        'key',
    ];
    public function status_pengaduan()
    {
        # code...
        return $this->hasMany(StatusPengaduanModel::class, 'pengaduan_id');
    }
    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
    public function category_pengaduan()
    {
        # code...
        return $this->belongsTo(CategoryPengaduanModel::class, 'category_pengaduan_id', 'id');
    }
}
