<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProfileModel extends Model
{
    //
    protected $table = 'profile';
    protected $fillable = [
        'users_id',
        'nomor_hp',
        'alamat',
        'foto',
        'jenis_kelamin',
        'pekerjaan',
        'nomor_nik',
        'role',
    ];
}
