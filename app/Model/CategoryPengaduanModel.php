<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryPengaduanModel extends Model
{
    protected $table = 'category_pengaduan';
    protected $fillable = [
        'judul',
    ];
}
