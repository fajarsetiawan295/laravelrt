<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UploadBuktiTagihanModel extends Model
{
    //
    protected $table = 'upload_bukti_tagihan';
    protected $fillable = [
        'foto',
        'nomor_rekening',
        'atas_nama',
        'tagihan_id',
        'status',
        'users_id',
        'key'
    ];

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function tagihan()
    {
        # code...
        return $this->belongsTo(TagihanModel::class, 'tagihan_id', 'id');
    }
}
