<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponeHelper;
use App\Http\Controllers\Controller;
use App\Model\BodySuratModel;
use App\Model\JenisSuratModel;
use App\Model\ProfileModel;
use App\Model\SuratModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PDF;

class Suratcontroller extends Controller
{
    //
    public function createJenisSurat(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $cre = JenisSuratModel::create([
            'nama' => $request->nama
        ]);

        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Jenis Surat');
    }

    public function ListJenisSurat()
    {
        # code...
        return ResponeHelper::GetDataBerhasil(JenisSuratModel::all());
    }

    public function BodySurat(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'rt' => 'required',
            'rw' => 'required',
            'kel' => 'required',
            'kec' => 'required',
            'kab' => 'required',
            'nama_rt' => 'required',
            'nama_rw' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $value =  ResponeHelper::cekKey();
        $data = $request->all();
        $data['key'] = $value;
        $cre = BodySuratModel::updateOrCreate(['key' => $value], $data);
        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Body Surat');
    }

    public function GetBodySurat(Request $request)
    {
        # code...
        $value =  ResponeHelper::cekKey();

        return ResponeHelper::GetDataBerhasil(BodySuratModel::where('key', $value)->first());
    }

    public function PengajuanSurat(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'jenis_surat_id' => 'required',
            'users_id' => 'required',
            'keterangan' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $value =  ResponeHelper::cekKey();
        $data = $request->all();
        $data['key'] = $value;
        $cre = SuratModel::create($data);
        ResponeHelper::fcmtoken(User::find($request->users_id)->email, 'Terimakasih Telah Membuat Surat', 'Tunggu Respone Dari RT untuk Surat anda', null);

        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Body Surat');
    }

    public function GetPengajuanSurat()
    {
        # code...
        $value =  ProfileModel::where('users_id', Auth::user()->id)->first();
        $ket =  ResponeHelper::cekKey();

        if ($value->role == 'RT') {

            return ResponeHelper::GetDataBerhasil(SuratModel::with('user')
                ->with('jenis_surat')
                ->with('body_surat')
                ->where('key', $ket)->get());
        }
        return ResponeHelper::GetDataBerhasil(SuratModel::with('user')
            ->with('jenis_surat')
            ->with('body_surat')
            ->where('users_id', Auth::user()->id)->get());
    }

    public function UpdateSurat($request)
    {
        # code...
        $cre = SuratModel::find($request)->update([
            'status' => 'Berhasil'
        ]);
        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Update Jenis Surat');
    }

    public function cetak_pdf($id)
    {
        # code...

        $pegawai = SuratModel::with('user.profile')
            ->with('jenis_surat')
            ->with('body_surat')
            ->where('id', $id)->first();

        $pdf = PDF::loadview('surat_pdf', ['pegawai' => $pegawai]);
        return $pdf->stream('laporan-surat-pdf');
    }
}
