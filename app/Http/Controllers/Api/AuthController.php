<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponeHelper;
use App\Http\Controllers\Controller;
use App\Model\ProfileModel;
use App\Model\RtModel;
use App\Model\TokenFcmModel;
use App\Model\WargaModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Str;


class AuthController extends Controller
{
    //
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed|min:6|max:8',
            'nomor_hp' => 'required|string',
            'alamat' => 'required|string',
            'jenis_kelamin' => 'required|string',
            'pekerjaan' => 'required|string',
            'role' => 'required|string',
            'nomor_nik' =>  'required|min:16|max:17',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        ProfileModel::create([
            'users_id' => $user->id,
            'nomor_hp' => $request->nomor_hp,
            'alamat' => $request->alamat,
            'jenis_kelamin' => $request->jenis_kelamin,
            'pekerjaan' => $request->pekerjaan,
            'nomor_nik' => $request->nomor_nik,
            'role' => $request->role,
        ]);

        return response()->json([
            'message' => 'Successfully created user!'
        ], 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|',
            'password' => 'required|string|',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $cekemail = User::where('email', $request->email)->first();
        if (!$cekemail) {
            return ResponeHelper::worngdata('Email Anda Belum Terdafar');
        }
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return ResponeHelper::worngdata('Password Anda Salah');

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        $cekwarga = WargaModel::where('users_id', Auth::user()->id)->where('status', 'Active')->first();
        $dat = true;
        $cekprof = ProfileModel::where('users_id', Auth::user()->id)->first();
        if ($cekprof->role == "Warga") {
            if (!$cekwarga) {
                $dat = false;
            }
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'status' => $dat,
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        }
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'status' => $dat,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function CreateRt(Request $request)
    {
        # code...
        if ($request->header('key') != 'qwertyuiop1234567890') {
            return ResponeHelper::badRequest('Key anda Salah');
        }
        $cekprofile = ProfileModel::where('users_id', Auth::user()->id)->first();
        if ($cekprofile->role == 'RT') {
            ResponeHelper::EncriptKeyCompany($request->header('key'), Auth::user()->id);
            $data = RtModel::updateOrCreate([
                'users_id'   => Auth::user()->id,
            ], [
                'key' =>  ResponeHelper::EncriptKeyCompany($request->header('key'), Auth::user()->id),
                'users_id' => Auth::user()->id,
            ]);

            WargaModel::updateOrCreate([
                'users_id'   => Auth::user()->id,
            ], [
                'users_id'  => Auth::user()->id,
                'type' => "RT",
                'key' => $data->key,
                'status' => "Active",
            ]);
            return ResponeHelper::CreteorUpdateBerhasil(null, 'Berhasil Create Rt');
        } elseif ($cekprofile->role == 'Warga') {
            # code...
            $validator = Validator::make($request->all(), [
                'keyRT' => 'required|string',
            ]);

            if ($validator->fails()) {
                return ResponeHelper::ResponValidator($validator);
            }

            $cekkey = RtModel::where('key', $request->keyRT)->first();
            if (!$cekkey) {
                return ResponeHelper::badRequest('Key Rt Tidak Ditemukan');
            }
            WargaModel::updateOrCreate([
                'users_id'   => Auth::user()->id,
            ], [
                'users_id'  => Auth::user()->id,
                'type' => "Warga",
                'key' => $request->keyRT,
                'status' => "Active",
            ]);
            return ResponeHelper::CreteorUpdateBerhasil(null, 'Berhasil Create Warga');
        }
        return ResponeHelper::badRequest('Anda Mendaftar Sebagai Rt');
    }

    public function CekKey()
    {
        # code...
        $cekprofile = ProfileModel::where('users_id', Auth::user()->id)->first();
        if ($cekprofile->role == 'RT') {
            $cek = RtModel::with('warga.users.profile')->where('users_id', Auth::user()->id)->first();
            if (!$cek) {
                return ResponeHelper::badRequest('Anda Belum Generet Key');
            }
            return ResponeHelper::GetDataBerhasil($cek);
        }
        $cekwarga = WargaModel::where('users_id', Auth::user()->id)->where('status', 'Active')->first();
        if (!$cekwarga) {
            return ResponeHelper::badRequest('Anda Belum Daftar Sebagai Warga RT ');
        }
        $cekall = RtModel::with('warga.users.profile')->where('key', $cekwarga->key)->first();
        return ResponeHelper::GetDataBerhasil($cekall);
    }

    public function nonaktifwarga(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'users_id' => 'required',
            'key' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $data = WargaModel::where('users_id', $request->users_id)->where('key', $request->key)->first();


        $emailuser = User::find($request->users_id);
        ResponeHelper::fcmtoken($emailuser->email, 'Telah Dinonaktifkan', 'Ada Telah Di Non Aktif kan Sebagai warga', null);

        $data->update([
            'status' => 'Non Active'
        ]);

        return ResponeHelper::CreteorUpdateBerhasil(null, 'Berhasil update Warga');
    }

    public function NonAktif()
    {
        # code...
        $cekprofile = ProfileModel::where('users_id', Auth::user()->id)->first();
        if ($cekprofile->role == 'RT') {
            $cek = RtModel::with('warga')->where('users_id', Auth::user()->id)->first();
            if (!$cek) {
                return ResponeHelper::badRequest('Anda Belum Generet Key');
            }
            $cekwarga = WargaModel::with('users')->where('key', $cek->key)->where('status', 'Non Active')->get();
            return ResponeHelper::GetDataBerhasil($cekwarga);
        }
        return ResponeHelper::badRequest('Anda Bukan Rt');
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        $cekuser = User::with('profile')->find(Auth::user()->id);
        return ResponeHelper::GetDataBerhasil($cekuser);
    }

    public function creatfcmtoken(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'token' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        TokenFcmModel::updateOrCreate([
            'email'   => $request->email,
        ], [
            'email'  => $request->email,
            'token' => $request->token,
        ]);
        return ResponeHelper::CreteorUpdateBerhasil(null, 'Berhasil Create token');
    }

    public function ResetPassword(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'email' => 'required',
            'password' => 'required|string|confirmed|min:6|max:8',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        // dd($credentials);
        if (Auth::user()->email != $request->email) {
            return ResponeHelper::NotFound('Email Yang di gunakan tidak sama dengan sesion login');
        }
        if (!Hash::check($request->old_password, Auth::user()->password)) {
            return ResponeHelper::NotFound('Pasword anda Tidak sama dengan sebelumnya');
        }
        $update = User::where('email', $request->email)->update([
            'password' =>  bcrypt($request->password)
        ]);
        // dd($update);
        return ResponeHelper::CreteorUpdateBerhasil($update, "berhasil Upadte");
    }




    public function UpdateFoto(Request $request)
    {
        # code...

        $validator = Validator::make($request->all(), [
            'foto' => 'required|file'
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/foto/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'foto-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);


        $cre = ProfileModel::where('users_id', Auth::user()->id)->update([
            'foto' => $imagePath . $filename,
        ]);

        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create');
        }

        return ResponeHelper::NotFound('Gagal Create');
    }
}
