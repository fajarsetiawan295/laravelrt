<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponeHelper;
use App\Http\Controllers\Controller;
use App\Model\CategoryPengaduanModel;
use App\Model\PengaduanModel;
use App\Model\StatusPengaduanModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Str;

class PengaduanController extends Controller
{
    //
    public function CreateCategori(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $cre = CategoryPengaduanModel::create([
            'judul' => $request->judul
        ]);

        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
    }

    public function lisCategory()
    {
        # code...
        return ResponeHelper::GetDataBerhasil(CategoryPengaduanModel::all());
    }

    public function Pengaduan(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'category_pengaduan_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/pengaduan/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'pengaduan-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['users_id'] = Auth::user()->id;
        $data['key'] = ResponeHelper::cekKey();
        $data['status'] = "Pending";

        $cre = PengaduanModel::create($data);
        ResponeHelper::fcmtoken(Auth::user()->email, 'Terimakasih Telah Melakukan Pengaduan', 'Tunggu Update Dari Rt', null);

        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }

    public function listPengauanRt()
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = PengaduanModel::with('category_pengaduan')
            ->with('user')
            ->with('status_pengaduan')
            ->where('key', $cekkey)->get();
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }
    public function listPengauanuser()
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = PengaduanModel::with('category_pengaduan')
            ->with('user')
            ->with('status_pengaduan')
            ->where('users_id', Auth::user()->id)
            ->where('key', $cekkey)->get();
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }

    public function DetailsPengaduan($id)
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = PengaduanModel::with('category_pengaduan')
            ->with('user')
            ->with('status_pengaduan')
            ->where('id', $id)
            ->where('key', $cekkey)->first();
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }



    public function UpdateStatus(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'pengaduan_id'  => 'required',
            'deskripsi'  => 'required',
            'foto'  => 'required',
            'status'  => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/pengaduan/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'pengaduan-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['users_id'] = Auth::user()->id;
        $cre = StatusPengaduanModel::create($data);

        if ($cre) {
            $pe = PengaduanModel::find($request->pengaduan_id);
            ResponeHelper::fcmtoken(User::find($pe->users_id)->email, 'Pengauan Anda Telah di respon', 'Ayo cek Pengaduan Anda', null);
            $cre = PengaduanModel::find($request->pengaduan_id)->update([
                'status' => $request->status
            ]);
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Update ');
        }
    }
}
