<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponeHelper;
use App\Http\Controllers\Controller;
use App\Model\CategoryModel;
use App\Model\Donasi_UploadModel;
use App\Model\InformasiModel;
use App\Model\KomentarInformasiModel;
use App\Model\ProfileModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Str;

class InformasiController extends Controller
{
    //
    public function CreateCategori(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $cre = CategoryModel::create([
            'judul' => $request->judul
        ]);

        return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
    }

    public function lisCategory()
    {
        # code...
        $value =  ResponeHelper::cekKey();

        $data = CategoryModel::with(['informasi' => function ($q) use ($value) {
            $q->where('key', '=', $value);
        }])
            ->get();

        // dd($data);
        return ResponeHelper::GetDataBerhasil($data);
    }

    public function informasi(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'foto' => 'required',
            'keterangan' => 'required',
            'judul_informasi' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/foto/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'foto-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['users_id'] = Auth::user()->id;
        $data['key'] = ResponeHelper::cekKey();

        $cre = InformasiModel::create($data);

        ResponeHelper::fcmtoken(null, 'Ada Informasi Baru', $request->judul_informasi, 'all');

        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }

    public function ListAllInformasi()
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = InformasiModel::with('category')
            ->with('user')
            ->where('key', $cekkey)->get();
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }

    public function ListInformasicategory($id)
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = InformasiModel::with('category')
            ->with('user')
            ->where('key', $cekkey)
            ->where('category_id', $id)
            ->get();
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }
    public function ListInformasiDetails($id)
    {
        # code...
        $cekkey =  ResponeHelper::cekKey();

        $cekinformasi = InformasiModel::with('category')
            ->with('user')
            ->with('komentar.user.profile')
            ->find($id);
        return ResponeHelper::GetDataBerhasil($cekinformasi);
    }

    public function komentar(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'informasi_id' => 'required',
            'komentar' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $data = $request->all();
        $data['users_id'] = Auth::user()->id;

        $cre = KomentarInformasiModel::create($data);

        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }

    public function transaksiUpload(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'category_id'  => 'required',
            'informasi_id'  => 'required',
            'norek'  => 'required',
            'atasnama'  => 'required',
            'foto'  => 'required|file',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/foto/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'foto-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['key'] = ResponeHelper::cekKey();
        $data['users_id'] = Auth::user()->id;


        $cre = Donasi_UploadModel::create($data);

        ResponeHelper::fcmtoken(Auth::user()->email, 'Terimakasi Telah Melakukan Donasi', 'Semoga Anda Mendapatkan Rezeki yang berlimpah', null);

        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }

    public function ListUpload()
    {
        # code...

        $value =  ProfileModel::where('users_id', Auth::user()->id)->first();
        $ket =  ResponeHelper::cekKey();

        if ($value->role == 'RT') {
            return ResponeHelper::GetDataBerhasil(Donasi_UploadModel::with('user')
                ->with('category')
                ->with('informasi')
                ->where('key', $ket)->get());
        }
        return ResponeHelper::GetDataBerhasil(Donasi_UploadModel::with('user')
            ->with('category')
            ->with('informasi')
            ->where('users_id', Auth::user()->id)->get());
    }
}
