<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponeHelper;
use App\Http\Controllers\Controller;
use App\Model\RekeningModel;
use App\Model\TagihanModel;
use App\Model\UploadBuktiTagihanModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Str;

class TagihanController extends Controller
{
    //
    public function CreateRekening(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'atas_nama' => 'required',
            'nomor_rekening' => 'required',
            'nama_bank' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $data = $request->all();
        $data['users_id'] = Auth::user()->id;
        $cre = RekeningModel::create($data);
        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }

    public function ListRekening(Request $request)
    {
        # code...
        return ResponeHelper::GetDataBerhasil(RekeningModel::where('users_id', Auth::user()->id)->get());
    }
    public function ListTagihan(Request $request)
    {
        # code...
        return ResponeHelper::GetDataBerhasil(TagihanModel::with('rekening')->where('key', ResponeHelper::cekKey())->get());
    }

    public function CreateTagihan(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'jenis_tagihan'  => 'required',
            'nominal'  => 'required',
            'tanggal'  => 'required',
            'rekening_id'  => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $data = $request->all();
        $data['key'] = ResponeHelper::cekKey();
        $cre = TagihanModel::create($data);
        if ($cre) {
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create Category');
        }
    }
    public function UploadBuktiPembayaran(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'foto' => 'required',
            'nomor_rekening' => 'required',
            'atas_nama' => 'required',
            'tagihan_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/tagihan/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'tagihan-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['key'] = ResponeHelper::cekKey();
        $data['users_id'] = Auth::user()->id;
        $data['status'] = 'Pending';

        $cre = UploadBuktiTagihanModel::create($data);
        if ($cre) {
            ResponeHelper::fcmtoken(Auth::user()->email, 'Terimakasih Telah Melakukan Pembayaran Tagihan', 'Rt Akan Mengecek Bukti Pembayaran', null);
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create');
        }
    }
    public function UploadBuktiPembayaranManual(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'users_id' => 'required',
            'foto' => 'required',
            'tagihan_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }

        $file = $request->file('foto');
        // dd($file);
        $imagePath = '/gambar/tagihan/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'tagihan-' . Str::random(16) . Auth::user()->name . '.' . $extension;
        $file->move($path, $filename);

        $data = $request->all();
        $data['foto'] = $imagePath . $filename;
        $data['key'] = ResponeHelper::cekKey();
        $data['users_id'] = $request->users_id;
        $data['status'] = 'Berhasil';

        $cre = UploadBuktiTagihanModel::create($data);
        if ($cre) {
            ResponeHelper::fcmtoken(User::find($request->users_id)->email, 'Terimakasih Telah Melakukan Pembayaran Tagihan', 'Pembayaran Anda Sudah Berhasil', null);
            return ResponeHelper::CreteorUpdateBerhasil($cre, 'Berhasil Create');
        }
    }
    public function ListTagihanUpload($status)
    {
        # code...
        $data = ResponeHelper::cekKey();

        $cek = UploadBuktiTagihanModel::with('user')
            ->with('tagihan')
            ->where('status', $status)
            ->where('key', $data)
            ->get();
        return ResponeHelper::GetDataBerhasil($cek);
    }

    public function UpdateStatusUpload(Request $request)
    {
        # code...
        $validator = Validator::make($request->all(), [
            'upload_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponeHelper::ResponValidator($validator);
        }
        $up = UploadBuktiTagihanModel::find($request->upload_id)->update([
            'status' => "Berhasil"
        ]);
        if ($up) {
            $u = UploadBuktiTagihanModel::find($request->upload_id);
            ResponeHelper::fcmtoken(User::find($u->users_id)->email, 'Terimakasih Telah Melakukan Pembayaran Tagihan', 'Tagihan Anda Sudah Di Aprove Oleh Rt', null);

            return ResponeHelper::CreteorUpdateBerhasil($up, 'Berhasil Update');
        }
    }

    public function UserUpload($user, $id)
    {
        # code...
        $data = ResponeHelper::cekKey();

        $cek = UploadBuktiTagihanModel::with('user')
            ->with('tagihan')
            ->where('tagihan_id', $id)
            ->where('users_id', $user)
            ->where('key', $data)
            ->get();
        return ResponeHelper::GetDataBerhasil($cek);
    }
}
