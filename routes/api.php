<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');
    Route::post('Create-FCM-Token', 'Api\AuthController@creatfcmtoken');
    Route::get('Cetak-Surat/{code}', 'Api\Suratcontroller@cetak_pdf');


    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
        Route::post('Create-Rt-warga', 'Api\AuthController@CreateRt');
        Route::get('Cek-warga-RT-active', 'Api\AuthController@CekKey');
        Route::get('Cek-warga-RT-nonactive', 'Api\AuthController@NonAktif');
        Route::post('Non-Active-Warga', 'Api\AuthController@nonaktifwarga');
        Route::post('Reset-Password', 'Api\AuthController@ResetPassword');
        Route::post('UpdateFoto', 'Api\AuthController@UpdateFoto');
    });
});
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'Informasi'
], function () {
    Route::post('Create-Categori', 'Api\InformasiController@CreateCategori');
    Route::post('Create-Informasi', 'Api\InformasiController@informasi');
    Route::post('komentar-Informasi', 'Api\InformasiController@komentar');
    Route::post('Upload-Donasi', 'Api\InformasiController@transaksiUpload');
    Route::get('lis-Categori', 'Api\InformasiController@lisCategory');
    Route::get('List-Donasi', 'Api\InformasiController@ListUpload');
    Route::get('List-Informasi-all', 'Api\InformasiController@ListAllInformasi');
    Route::get('List-Informasi-category/{id}', 'Api\InformasiController@ListInformasicategory');
    Route::get('List-Informasi-Details/{id}', 'Api\InformasiController@ListInformasiDetails');
});

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'Pengaduan'
], function () {
    Route::post('Create-Categori', 'Api\PengaduanController@CreateCategori');
    Route::post('Create-Pengaduan', 'Api\PengaduanController@Pengaduan');
    Route::get('lis-Categori', 'Api\PengaduanController@lisCategory');
    Route::get('list-Pengauan-Rt', 'Api\PengaduanController@listPengauanRt');
    Route::get('list-Pengauan-user', 'Api\PengaduanController@listPengauanuser');
    Route::post('Update-Status', 'Api\PengaduanController@UpdateStatus');
    Route::get('Details-Pengaduan/{id}', 'Api\PengaduanController@DetailsPengaduan');
});


Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'Tagihan'
], function () {
    Route::post('Create-Rekening', 'Api\TagihanController@CreateRekening');
    Route::get('List-Rekening', 'Api\TagihanController@ListRekening');
    Route::post('Create-Tagihan', 'Api\TagihanController@CreateTagihan');
    Route::get('List-Tagihan', 'Api\TagihanController@ListTagihan');
    Route::post('Upload-Bukti-Pembayaran', 'Api\TagihanController@UploadBuktiPembayaran');
    Route::post('Upload-Bukti-Pembayaran-Manual', 'Api\TagihanController@UploadBuktiPembayaranManual');
    Route::post('Update-Status-Upload', 'Api\TagihanController@UpdateStatusUpload');
    Route::get('List-Tagihan-Upload/{code}', 'Api\TagihanController@ListTagihanUpload');
    Route::get('User-Upload/{user}/{tagihan}', 'Api\TagihanController@UserUpload');
});
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'Surat'
], function () {
    Route::post('create-Jenis-Surat', 'Api\Suratcontroller@createJenisSurat');
    Route::post('Body-Surat', 'Api\Suratcontroller@BodySurat');
    Route::get('List-Jenis-Surat', 'Api\Suratcontroller@ListJenisSurat');
    Route::get('Get-Body-Surat', 'Api\Suratcontroller@GetBodySurat');
    Route::post('Pengajuan-Surat', 'Api\Suratcontroller@PengajuanSurat');
    Route::get('Get-Pengajuan-Surat', 'Api\Suratcontroller@GetPengajuanSurat');
    Route::get('Update-Surat/{code}', 'Api\Suratcontroller@UpdateSurat');
});
